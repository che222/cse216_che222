# CSE 216

This is an individual student repository. It is intended for use during phase 0.

## Details

- Semester: Spring 2021
- Student ID: che222
- Bitbucket repository: "https://che222@bitbucket.org/che222/cse216_che222.git"

## Contributers

1. Cabe Edelhertz

## Note for grading

- In my file organization, I was supposed to have the maven make a path of "src/main/java/edu/lehigh/cse216/che222/..." but I accidentally capitalized the L in Lehigh, so my whole project uses the the path "src/main/java/edu/Lehigh/cse216/che222/...". Hope that doesn't cause too much confusion.
- For task 2, updating the backend to use the database, my code connects to the database and creates a table both from the backend folder rather than the admin. This can cause some warnings for creating duplicate named tables, however it should still work. Better practice in the future would be to create table in admin.
For now, it should all work fine if you "docker run" the database and then execute the backend code, which will connect to the database and create a table.
